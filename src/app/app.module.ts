import { ErrorHandler, Injectable, NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import * as Sentry from "@sentry/browser";
import { environment } from "src/environments/environment";
import { AppComponent } from "./app.component";

Sentry.init({
  dsn: "https://478fb5d14bd543f7a10b38aeb8d8ed53@sentry.io/1392971",
  release: environment.version
});

@Injectable()
export class SentryErrorHandler implements ErrorHandler {
  constructor() {}
  handleError(error) {
    Sentry.captureException(error.originalError || error);
    throw error;
  }
}

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule],
  providers: [{ provide: ErrorHandler, useClass: SentryErrorHandler }],
  bootstrap: [AppComponent]
})
export class AppModule {}
